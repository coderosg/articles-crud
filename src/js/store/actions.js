import api from '../utils/api'

export default {
    fetchArticles(store, articles) {
        return api.get('articles')
            .then(response => {
                store.commit('SET_ARTICLES', response.data)
            })
    },
    postArticle(store, article) {
        return api.post('add-article', article)
            .then(response => {
                store.commit('SET_ARTICLES', response.data)
            })
    },
    removeArticle(store, id) {
        return api.delete(`articles/${id}`)
            .then(response => {
                store.commit('SET_ARTICLES', response.data)
            })
    }
}