import axios from 'axios'

const api = axios.create({
    baseURL: 'https://tr-logic-test.herokuapp.com/api/'
})

export default api